maketable<-function(mylist)
{
  #this function make a data table with all the info of the passed RADAR object
  #it contains "lotid"     "tester_id" "wafer_id"  "x_coord"   "y_coord"   "variable"  "value" 
  
  char<- as.character(c(mylist$LotInfoFrame$lotid, mylist$LotInfoFrame$tester_id, mylist$WafersFrame$wafer_id))
  length<-nrow(mylist$ResultsMatrix)
  info<-data.frame(rep(char,times=length),nrow=length,byrow=TRUE )
  
  
  colnames(info)<-c("lotid", "tester_id", "wafer_id")
  #take all general information  
  
  rawdata<-data.table(mylist$ResultsMatrix)   
  #take test result 
  
  colnames(rawdata)<-as.character(mylist$ParametersFrame$testname)
  #combine test result with test name
  
  testneeded=c("TN120","TN140","TN160","TN180","Perr", "0-PH.*S21p")
  # search for pattern, take only the tests we need,  since TN1200 is included in TN120, we don't need to repeat
  
  indt<-colnames(rawdata) %like% paste(testneeded,collapse = "|")
  # extract testneeded from data
  
  info<-info[rep(1,times=nrow(rawdata)),] 
  ###a bit wordy, but dulicate "info" to the number of rows in rawdata, so they can be pasted together
  
  final<-cbind(info,mylist$DevicesFrame[,c("x_coord","y_coord")],rawdata[,..indt])
  #bind lotinfo wafer info, die info, and parameter result all together
  final2<- melt(final, id=c("lotid", "tester_id", "wafer_id", "x_coord","y_coord"), variable.name="testname")
  #melt into 1 test per row
  
  
  return(final2)
}  
addunit<-function(x)#this function add units to plot base on passing parm name
{  
  if (x=="S21m")
    return("S21 (dB)")
  else if (x=="P1dB")
    return("P1dB (dBm)")
  else if (x=="Perr")
    return("Phase Error (degree)")
  else if (x=="S21p")
    return("Phase Reference (degree)")
  else if (x=="RefErr")
    return("Phase Reference Error (degree)")
  else
    return("Unknown")
  
}

phasewarpping<-function(x)
{
  if (x>0)
  {x<-x%%360}
  else 
  {x<-x%%-360}
  if (x>180)
  {x<-(x-360)}
  else if (x<(-180))
  {x<-(x+360)}  
  return(x)
}
phasewarppingvector<-function(x)
{
    x<-x%%(360*sign(sign(x)+.5))
    ind<-abs(x)>180
    x[ind]<-x[ind]-sign(x[ind])*360
    return(x)
}