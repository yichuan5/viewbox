This is a web app developed for mercury product under the request of Kaushal Kannan. 



This app have perform following functions:

1. retrieve and filter GMIW data test data from server.

2. convert STDF data into R dataTables or CSV format.

3. regroup and join of STDF data from different file.

4. interactive visualization of data statistics.

![Demo Picture](/demo.png)
