options(shiny.maxRequestSize=1000*1024^2) ##increase file upload size
options(warn=-1)##supress warning made by convertSTDF
options(spinner.color.background="#F5F5F5") ## match background color
source("ConvertStdf.R")
source("addon.r")
library(shinythemes)
library(shiny)
library(plotly)
library(ROracle)
library(data.table)
library(shinycssloaders)
library(rhandsontable)
library(processx)
library(shinyjs)
ui <- {navbarPage(
  theme = shinytheme("flatly"),
  "Parametric Distribution",# Application title
  tabPanel('File Import and Display',sidebarLayout(
    sidebarPanel(width=3,
                 textInput("lotid", "Lot ID", value="AA0456056.00F"),
                 numericInput("days", "Days from today","30"),
                 selectInput("rxtx", "RX/TX", c("RX"="WAFER_TEST1.1", "TX"="WAFER_TEST2.1"),selected="WAFER_TEST2.1"),
                 selectInput("compare", "Group", c("Lot"="lotid", "Wafer"="wafer_id"),selected="wafer_id"),
                 actionButton("fetch", "GMIW Retrieve"),
                 br(),br(),
                 fileInput("file","Select STDF Files",multiple = TRUE, accept = ".STDF"),
                 h5(tags$b("Save State")),
                 downloadButton("downloadData", "Save"),
                 fileInput("rdata","Load State",multiple=FALSE, accept=".RDATA")),
    mainPanel(useShinyjs(),         
      div(style="display: inline-block;vertical-align:top; width: 120px;",selectInput("parm", "Parameter", c("Gain"="S21m", "P1dB"="P1dB", "Phase Error"="Perr", "Abs. Phase"="S21p", "Quad. Offset"="RefErr"),selected="S21m")),
      div(style="display: inline-block;vertical-align:top; width: 120px;",selectInput("freq", "Frequency", c("all"=".","36G"="36000", "36.5G"="36500","37G"="37000","37.5G"="37500","38G"="38000","38.5G"="38500", "39G"="39000","39.5G"="39500","40G"="40000"),selected=".")),
      div(style="display: inline-block;vertical-align:top; width: 120px;",selectInput("quad", "Quadrant", c("all"=".","NW"="((TN120)|(TN2))", "NE"="((TN140)|(TN4))","SE"="((TN160)|(TN6))","SW"="((TN180)|(TN8))"),selected=".")),
      div(style="display: inline-block;vertical-align:top; width: 120px;",selectInput("filt", "Filtering", c("off"=1,"6 Sigma"=0.99999,"3 Sigma"=0.99863,"2 Sigma"=0.97725,"1.5 Sigma"=0.93319),selected=1)),
      div(style="display: inline-block;vertical-align:top; width: 120px;",selectInput("delay", "Delay Offset", c("all"=".","5.625\u00B0"="P1-", "11.25\u00B0"="P2-","22.5\u00B0"="P4-","45\u00B0"="P8-", "90\u00B0"="P16-","180\u00B0"="P32-"),selected=".")),
      withSpinner(plotlyOutput("plotmain",height = 700, width = 1024),type=2))
    
  )),
  tabPanel('Customize Grouping',id="cust",
           h4("GMIW Imported Data"),rHandsontableOutput("hotgmiw"),
           h4("STDF Imported Data"),rHandsontableOutput("hotstdf"),
           actionButton("change","Change Grouping"))
)}

server <- function(input, output,session) {
  #############declaration#################
  session$onSessionEnded(stopApp)       #stop app when session end
  pmargin<-list(t=0,b=200,r=50,l=50)    #margin for plotly
  userin<-list("days","lotid", "compare","rxtx") #single call to combine three input
  userin$days<-  eventReactive(input$fetch, input$days)
  userin$lotid<- eventReactive(input$fetch, input$lotid)
  userin$compare<-eventReactive(input$fetch, input$compare)
  userin$rxtx<-eventReactive(input$fetch, input$rxtx)
  savelist<-NULL                        #declare load temp file
  tgmiw <- reactiveValues()             #user view gmiw table
  toverview<-reactiveValues()           #full datatable
  tstdf<-reactiveValues()               #user view stdf table
  tgmiw$data<-data.frame(lotid=character(), group=factor(levels=c("lotid","wafer_id")),inputorder=integer()) #filename and their group for GMIW
  tstdf$data<-data.frame(filename=character(), group=character(),inputorder=integer()) #filename and their group for STDF
  toverview$index<-0                    #inputorder index
  toverview$result<-data.table("lotid"=character(),"wafer_id"=character(),	"x_coord"=numeric(),"y_coord"=numeric(),"testname"=character(),"value"=numeric(),"group"=character(),"inputorder"=integer()) #combined table
  #############SQL retrieve#################################################
  sqlin<-observeEvent(input$fetch,{
    sql<-paste("SELECT  /*+ OPT_PARAM('optimizer_dynamic_sampling' 0) LEADING(seed, testLimit, anaUnitResults) USE_NL(seed, testLimit, anaUnitResults) NO_MERGE(seed) */  
               lot as lotid, wafer_id, x_Coordinate as x_coord, y_Coordinate as y_coord, test_Name as testname, result_Value as value
               FROM (SELECT  /*+ OPT_PARAM('optimizer_dynamic_sampling' 0) LEADING(wafSummary, testRunSetup, testInsertion) USE_NL(wafSummary, testRunSetup, testInsertion) */  

               productGroup.product_Group_Id, testInsertion.product_Id,testInsertion.lot_Id as lot, wafer.vendor_Scribe as wafer_id,testInsertion.mfg_Step_Name as step, wafSummary.test_Run_SK, wafSummary.wafer_SK , testRunSetup.test_Limit_Set_SK
               FROM Sapphire.Waf_Summary wafSummary,Sapphire.Test_Insertion testInsertion,Sapphire.Test_Run_Setup testRunSetup,Sapphire.Wafer wafer,Sapphire.Product product, Sapphire.Product_Group productGroup,Sapphire.Lot_State lotState
               WHERE wafer_test_date >= sysdate-",userin$days()," and testInsertion.lot_SK = lotState.lot_SK (+) and testInsertion.MFG_STEP_NAME = '",userin$rxtx(),"' and testInsertion.LOT_ID = '",userin$lotid(), "' and latest_Data_Flag in ('Y', 'N') and TECHNOLOGY = '45RFSOI'  and testInsertion.insertion_Type = 'SORT'
               and wafSummary.test_Run_SK = testRunSetup.test_Run_SK and testRunSetup.test_Insertion_SK = testInsertion.test_Insertion_SK and wafSummary.wafer_SK = wafer.wafer_SK and testInsertion.mfg_Area_SK = product.mfg_Area_SK and testInsertion.product_SK = product.product_SK and productGroup.product_Group_SK = product.product_Group_SK ) 
               seed, Sapphire.Unit unit, Sapphire.Sort_Ana_Unit_Results anaUnitResults, Sapphire.Test_Limit_Set testLimitSet, Sapphire.Test_Limit testLimit
               WHERE anaUnitResults.wafer_SK = seed.wafer_SK and anaUnitResults.test_run_SK = seed.test_run_SK and anaUnitResults.test_Limit_SK = testLimit.test_Limit_SK and anaUnitResults.unit_SK = unit.unit_SK and unit.wafer_SK = seed.wafer_SK and testLimitSet.test_Limit_Set_SK = seed.test_Limit_Set_SK and testLimit.test_Limit_Set_SK = seed.test_Limit_Set_SK and 
               
               (test_Name LIKE '%TN120%' OR test_Name LIKE '%TN140%' OR test_Name LIKE '%TN160%' OR test_Name LIKE '%TN180%' OR test_Name LIKE '%Perr' OR test_name LIKE '%0-PN%S21p')
               ORDER by lot",sep="")
    
    withProgress(message = 'Establish SQL connection',value = 0,{
      saph_conn <- dbConnect(dbDriver("Oracle"),
                             username="YLu5",          #my account
                             password="changeme",      #my password
                             dbname="(DESCRIPTION=(FAILOVER=on)(LOAD_BALANCE=on)(ADDRESS=(PROTOCOL=TCP)(HOST=fc8dbexa02s)(PORT=1521))(CONNECT_DATA=(SERVICE_NAME=gmiwp1)))")
      
      incProgress(0.1, message = 'Retrieving Data from Database') #get, save, disconnect
      dataRetrieved <- dbGetQuery(saph_conn, sql)
      if(nrow(dataRetrieved)>0){                                #empty retrieval check
        mytable<-data.table(dataRetrieved)
        dbDisconnect(saph_conn)  ##disconnect
        
        incProgress(0.8, message = 'Computing Data')                #bind, indexing
        colnames(mytable)<- tolower(colnames(mytable))          #change to lower case
        toverview$index<-toverview$index+1                      #indexing all rows
        mytable$inputorder<-rep(toverview$index, nrow(mytable))
        mytable$group<-  mytable[,get(userin$compare())]                                  # add compare group
        toverview$result<-rbindlist(list(toverview$result,mytable),use.names =TRUE)                                         #result table bind
        tgmiw$data<-rbind(tgmiw$data, data.frame(lotid=mytable$lotid[1], group=input$compare, inputorder=toverview$index))  #user table bind
      }
      #else{}
      incProgress(0.8, message = 'Done')
    })})
  #############Load stdf file##########################################################
  observeEvent(input$file,{
    progress <- shiny::Progress$new()               #add progress bar
    progress$set(message = "Loading", value = 0)
    on.exit(progress$close())                       #Close the progress 
    #################parse file/convert from STDF#########################
    mytable<-NULL
    inFile<-input$file
    
    for(i in 1:nrow(inFile)){###allow multiple file input
      mylist<-ConvertStdf(stdf_name = basename(inFile$datapath[i]), stdf_dir=dirname(inFile$datapath[i]),  updateProgress=progress)
      ##convert from STDF to a list
      temptable<-maketable(mylist)
      ##combine lists made my convertstdf to make a single table
      temptable<-temptable[,c("lotid","wafer_id",	"x_coord","y_coord","testname","value")]## remove other columns(tester_id) 
      toverview$index<-toverview$index+1                  #increase inputorder index
      temptable$inputorder<-toverview$index               #copy inputorder index
      temptable$group<-as.character(toverview$index-i+1)  #group number should be the same across multiple file input
      tstdf$data<- rbind(tstdf$data,data.frame(filename=inFile$name[i], group=as.character(toverview$index-i+1),inputorder=toverview$index) )
      ##bind the name & group indentifier into a table for user edit
      mytable<-rbind(mytable,temptable)
      #bind input table with overal table
      mytable<-data.table(mytable)
    }
    toverview$result<-rbindlist(list(toverview$result,mytable),use.names =TRUE)
  })
  #############Load Rdata#############################################################
  observeEvent(input$rdata,{
    inFile<-input$rdata
    load(inFile$datapath)
    toverview$result<-saveData$toverview
    toverview$index<-saveData$index
    tgmiw$data<-saveData$tgmiw
    tstdf$data<-saveData$tstdf
  })
  #############table edit###############################################################
  observeEvent(input$hotgmiw,{
    if (!is.null(input$hotgmiw)) {tgmiw$data[,c("lotid","group")] = hot_to_r(input$hotgmiw)} })
  observeEvent(input$hotstdf,{
    if (!is.null(input$hotstdf)) {tstdf$data[,c("filename","group")] = hot_to_r(input$hotstdf)} })
  #############change group##########################################################
  observeEvent(input$change,{
    withProgress(message = 'changing group',value=0,{
      if(nrow(tgmiw$data)>0){
        added<-sapply(1:nrow(tgmiw$data),
                      function(i){a<-(tgmiw$data$group[i])
                      toverview$result[toverview$result$inputorder==tgmiw$data$inputorder[i],..a]}
        )
        
        toverview$result[toverview$result$inputorder %in% tgmiw$data$inputorder,]$group<-unlist(added)
      }
      incProgress(.5)
      if(nrow(tstdf$data)>0){
        sapply(1:nrow(tstdf$data), function(i){
          toverview$result[toverview$result$inputorder==tstdf$data$inputorder[i],]$group<-tstdf$data$group[i]
          return(0)
        })
        
      }})
  })
  ############enable/disable menu items########################################################
  observeEvent(input$parm,{
    if (input$parm=="Perr")
    {shinyjs::enable("delay")}
    else
    {
      shinyjs::reset("delay")
      shinyjs::disable("delay")
    }
  })
  observeEvent(input$parm,{
    if (input$parm=="RefErr")
    {shinyjs::reset("filt")
      shinyjs::disable("filt")}
    else
    {
      shinyjs::enable("filt")
    }
  })
 
  #############plot render###############################################################
  output$plotmain<-renderPlotly({
    withProgress(message = 'Filtering', value = .1,{
      
      
      if(input$parm=="RefErr" ) #plot phase reference
      {
        pattern<-paste(input$freq,"S21p",sep=".*")
        ind<-grepl(pattern,toverview$result$testname)
        p<-toverview$result[ind,]
        ind2<-toverview$result[ind,]$testname %like% c("NW0")   #NW0 as keyword to grep reference
        if (input$quad==".")
        {p$value<-p$value-p[ind2,rep(value,4),by=inputorder]$V1} 
        #if all quant plot, NW needs to repeat 4 times, grouped by inputorder to resolve multiple files 
        else
        {
          q<-data.table(p)
          ind3<-grepl(input$quad, p$testname)
          p<-p[ind3,]
          p$value<-p$value-q[ind2,]$value
        }
        
         p<-data.table(p)
         # ind4<-p[,value<quantile(value,as.numeric(input$filt))&value>quantile(value,1-as.numeric(input$filt)),by=c("testname","group")]$V1 #remove outlier
         # p<-p[ind4,]
      }else #other plot filter
      {
        pattern<-paste(input$delay,input$quad,input$freq,input$parm,sep=".*")
        ind<-grepl(pattern, toverview$result$testname)
        p<-toverview$result[ind,]
        ind2<-p[,value<=quantile(value,as.numeric(input$filt))&value>=quantile(value,1-as.numeric(input$filt)),by=c("testname","group")]$V1 #remove outlier
        p<-p[ind2,]
      }

      p$testname<-factor(p$testname,levels = p$testname,ordered = TRUE) #prevent re-ordering of testname by plotly
      
      if(input$parm!="S21m"&input$parm!="P1dB")##phase wrapper
      {
        p$value<-phasewarppingvector(p$value)
      }
      
      yax <- list(title = addunit(input$parm),zeroline = FALSE)
      xax <- list(title = "Test Names",zeroline = FALSE,showline = FALSE, showgrid = FALSE )
      
      
      plotly<-plot_ly(p, x = p$testname, y = p$value, color =p$group, type = "box") %>%layout(showlegend=TRUE,boxmode = "group",hovermode='compare',yaxis=yax,xaxis=xax,margin = pmargin)
      
      incProgress(amount=.5,message='Almost Done')
      plotly
    })})
  #############download state###########################################################
  output$downloadData <- downloadHandler(filename = function() {paste("r", Sys.Date(), ".RDATA", sep="")},
                                         content = function(file){
                                           saveData<-list(toverview=toverview$result,tgmiw=tgmiw$data,tstdf=tstdf$data,index=toverview$index)
                                           save(saveData,file=file)
                                         }
  )
  #############render group table#####################################################
  output$hotgmiw <- renderRHandsontable({
    DF <- data.frame(subset(tgmiw$data, select=c("lotid","group")))
    DF$group <- factor(DF$group,levels=c("lotid","wafer_id"))
    if (!is.null(DF))
      rhandsontable(DF, contextMenu = FALSE,colHeaders=c("lot ID", "Group by"),width=1000,height=200,useTypes = TRUE,readOnly = TRUE, allowInvalid=FALSE) %>%hot_col(2, readOnly = FALSE)
  })
  output$hotstdf <- renderRHandsontable({
    DT <- data.frame(filename=tstdf$data$filename, group=as.character(tstdf$data$group),stringsAsFactors = FALSE)
    if (!is.null(DT))
      rhandsontable(DT, contextMenu = FALSE,colHeaders=c("File Name", "Group"),width=1000,height=200,useTypes = TRUE,readOnly = TRUE, allowInvalid=TRUE) %>%hot_col(2, readOnly = FALSE)
  })
  
}
#######################DONE################################################
shinyApp(ui = ui, server = server)
